; Copyright (c) 2021, RISC OS Open Ltd
; All rights reserved.
;
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:
;     * Redistributions of source code must retain the above copyright
;       notice, this list of conditions and the following disclaimer.
;     * Redistributions in binary form must reproduce the above copyright
;       notice, this list of conditions and the following disclaimer in the
;       documentation and/or other materials provided with the distribution.
;     * Neither the name of RISC OS Open Ltd nor the names of its contributors
;       may be used to endorse or promote products derived from this software
;       without specific prior written permission.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
; POSSIBILITY OF SUCH DAMAGE.
;

        GBLL    standalone
standalone SETL {TRUE}

        GET     ../../../aborttrap/atcontext.s

; Veneer for ABT vector
        GET     ../../../Copro15ops.hdr
        EXPORT  veneer
veneer ROUT
        SUB     sp, sp, #abtcontext_SPSR_svc
        STMIA   sp, {r0-r12}
        ADD     r0, sp, #abtcontext_SPSR
        ADD     r1, sp, #abtcontext_SPSR_svc
        STMDB   r0, {r1,r14}
        MRS     r1, SPSR
        ARM_read_FAR r2
        ARM_read_FSR r3
        STMIA   r0,{r1-r3}
        MOV     r0, sp
        LDR     r12, ws_ptr
        BL      aborttrap_dataabort_veneer
        CMP     r0, #0
        LDREQ   r4, [sp, #abtcontext_SPSR]
        MSREQ   SPSR_cxsf, r4
        LDMEQIA sp, {r0-r13,r15}^         ; The recovered R13 should discard the everything else from the stack
        ADD     r0, sp, #abtcontext_SPSR
        LDMIA   r0, {r1-r3}
        MSR     SPSR_cxsf, r1
        ARM_write_FAR r2
        ARM_write_FSR r3
        LDMIA   sp, {r0-r14}
        LDR     pc, old_handler

        EXPORT  veneer_config
veneer_config
        STR     r0, ws_ptr
        STR     r1, old_handler
        MOV     pc, lr

ws_ptr
        DCD     0
old_handler
        DCD     0

        END
